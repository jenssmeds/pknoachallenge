﻿using Domain.Transaction;

namespace Application.Commands.AddTransaction
{
    public class AddTransactionCommand
    {
        readonly ITransactionRepository transactionRepository;
        readonly IEventBus eventBus;

        public AddTransactionCommand(ITransactionRepository transactionRepository, IEventBus eventBus)
        {
            this.transactionRepository = transactionRepository;
            this.eventBus = eventBus;
        }

        public void Execute(AddTransactionRequest request)
        {
            var transaction = new TransactionFactory().Create(request.UserId, request.Value);
            transactionRepository.Save(transaction);
            eventBus.Push(request, this);
        }
    }
}
