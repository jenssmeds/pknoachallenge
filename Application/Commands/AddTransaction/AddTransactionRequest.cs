﻿using System;

namespace Application.Commands.AddTransaction
{
    public class AddTransactionRequest
    {
        public AddTransactionRequest(Guid userId, double value)
        {
            UserId = userId;
            Value = value;
        }

        public Guid UserId { get; private set; }
        public double Value { get; private set; }
    }
}
