﻿using System;

namespace Application
{
    public interface IEventBus
    {
        void Push<T>(T e, object sender);
        void Subscribe<T>(Action<IEvent> action);
    }

    public interface IEvent
    {
        object Object { get; }
        object Sender { get; }
    }
}
