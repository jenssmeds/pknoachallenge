﻿using System;

namespace Application.Queries.Transaction
{
    public class TransactionResponse
    {
        public Guid Id { get; set; }
        public Guid UserId { get; set; }
        public double Value { get; set; }
    }
}
