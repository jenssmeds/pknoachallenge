﻿using System.Collections.Generic;
using System.Linq;
using Domain.Transaction;

namespace Application.Queries.Transaction
{
    public class TransactionQuery
    {
        readonly ITransactionRepository transactionRepository;

        public TransactionQuery(ITransactionRepository transactionRepository)
        {
            this.transactionRepository = transactionRepository;
        }

        public virtual IEnumerable<TransactionResponse> GetAll()
        {
            var transactions = transactionRepository.GetAll();
            return transactions.Select(tx => new TransactionResponse() {Id = tx.Id, UserId = tx.UserId, Value = tx.Value});
        }
    }
}
