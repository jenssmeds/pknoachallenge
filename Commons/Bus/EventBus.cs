﻿using System;
using Application;

namespace Commons.Bus
{
    public class EventBus : IEventBus
    {
        readonly TinyMessengerHub hub;

        public EventBus()
        {
            hub = new TinyMessengerHub();
        }

        public void Push<T>(T o, object sender)
        {
            var message = new Event<T>(o, sender);
            hub.PublishAsync(message);
        }

        public class Event<T> : ITinyMessage, IEvent
        {
            public Event(object o, object sender)
            {
                Object = o;
                Sender = sender;
            }

            public object Object { get; private set; }
            public object Sender { get; private set; }
        }

        public void Subscribe<T>(Action<IEvent> action)
        {
            hub.Subscribe<Event<T>>(action);
        }
    }
}
