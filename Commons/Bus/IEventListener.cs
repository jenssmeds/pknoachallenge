﻿namespace Commons.Bus
{
    public interface IEventListener
    {
        void Subscribe();
    }
}
