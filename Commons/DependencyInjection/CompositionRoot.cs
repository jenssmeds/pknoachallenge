﻿using Application;
using Commons.Bus;
using Domain.Transaction;
using Microsoft.Practices.Unity;
using Persistence;
using ServiceStack.Redis;

namespace Commons.DependencyInjection
{
    public class CompositionRoot
    {
        public virtual void ConfigureServices(IUnityContainer container)
        {
            container.RegisterType<IEventBus, EventBus>(new ContainerControlledLifetimeManager());
            container.RegisterType<ITransactionRepository, TransactionRepository>(new ContainerControlledLifetimeManager());

            var redisClient = new RedisClient("localhost");
            container.RegisterInstance<IRedisClient>(redisClient);
        }
    }
}
