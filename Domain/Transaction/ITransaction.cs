﻿using System;

namespace Domain.Transaction
{
    public interface ITransaction
    {
        Guid Id { get; set; }
        Guid UserId { get; set; }
        double Value { get; set; }
    }
}
