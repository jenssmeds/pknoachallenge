﻿using System.Collections.Generic;

namespace Domain.Transaction
{
    public interface ITransactionRepository
    {
        IEnumerable<ITransaction> GetAll();
        void Save(ITransaction transaction);
    }
}
