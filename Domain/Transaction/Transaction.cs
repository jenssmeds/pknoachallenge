﻿using System;

namespace Domain.Transaction
{
    class Transaction : ITransaction
    {
        public int ExternalId { get; set; }
        public Guid Id { get; set; }
        public Guid UserId { get; set; }
        public double Value { get; set; }
    }
}
