﻿using System;

namespace Domain.Transaction
{
    public class TransactionFactory
    {
        public virtual ITransaction Create(Guid userId, double value)
        {
            return new Transaction()
            {
                Id = Guid.NewGuid(),
                UserId = userId,
                Value = value
            };
        }
    }
}
