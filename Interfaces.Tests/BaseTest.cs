﻿using Domain.Transaction;
using Microsoft.Owin.Builder;
using Microsoft.Practices.Unity;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ServiceStack.Redis;

namespace Interfaces.Tests
{
    [TestClass]
    public abstract class BaseTest
    {
        [AssemblyInitialize]
        public static void AssemblyInitialize(TestContext context)
        {
            new TestStartup().Configuration(new AppBuilder());

            UnityConfig.Container.Resolve<IRedisClient>().DeleteAll<ITransaction>();
        }
    }
}
