﻿using System.Collections.Generic;
using Interfaces.Controllers;
using Interfaces.Dtos;
using Microsoft.Practices.Unity;

namespace Interfaces.Tests.Helpers
{
    class ATestUser
    {
        internal static IEnumerable<TransactionDto> GetsAllTransactions()
        {
            return UnityConfig.Container.Resolve<TransactionsController>().Get();
        }

        public static void AddsTransaction(TransactionDto transaction)
        {
            var transactionController = UnityConfig.Container.Resolve<TransactionsController>();
            transactionController.Write(transaction);
        }
    }
}
