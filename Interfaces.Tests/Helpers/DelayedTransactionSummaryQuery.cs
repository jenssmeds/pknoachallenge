﻿using System.Collections.Generic;
using System.Threading;
using Application.Queries.Transaction;
using Domain.Transaction;

namespace Interfaces.Tests.Helpers
{
    class DelayedTransactionQuery : TransactionQuery
    {
        readonly int delay;

        public DelayedTransactionQuery(ITransactionRepository transactionRepository, int delay)
            : base(transactionRepository)
        {
            this.delay = delay;
        }

        public override IEnumerable<TransactionResponse> GetAll()
        {
            Thread.Sleep(delay);
            return base.GetAll();
        }
    }
}
