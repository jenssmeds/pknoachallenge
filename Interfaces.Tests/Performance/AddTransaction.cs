﻿using System;
using Application.Queries.Transaction;
using Domain.Transaction;
using Interfaces.Dtos;
using Interfaces.Tests.Helpers;
using Microsoft.Practices.Unity;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Interfaces.Tests.Performance
{
    [TestClass]
    public class AddTransaction : BaseTest
    {
        const int ExecutionDelayInMilliseconds = 2000;

        TransactionDto transaction;

        [TestInitialize]
        public void Initialize()
        {
            transaction = new TransactionDto() { Transaction = 42, UserId = Guid.NewGuid() };
            UnityConfig.Container.RegisterInstance<TransactionQuery>(new DelayedTransactionQuery(UnityConfig.Container.Resolve<ITransactionRepository>(), ExecutionDelayInMilliseconds));
        }

        [TestMethod]
        public virtual void IsNotAffectedByClientsListeningToTheEvent()
        {
            var startTime = DateTime.Now;
            ATestUser.AddsTransaction(transaction);
            var endTime = DateTime.Now;

            var executionTime = endTime - startTime;
            Assert.IsTrue(executionTime.TotalMilliseconds < ExecutionDelayInMilliseconds);
        }
    }
}
