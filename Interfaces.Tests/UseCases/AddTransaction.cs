﻿using System;
using System.Linq;
using Interfaces.Dtos;
using Interfaces.Tests.Helpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Interfaces.Tests.UseCases
{
    [TestClass]
    public class AddTransaction : BaseTest
    {
        [TestMethod]
        public virtual void IncreasesTheTransactionSum()
        {
            var dto1 = new TransactionDto() {Transaction = 42, UserId = Guid.NewGuid()};
            var dto2 = new TransactionDto() {Transaction = 23, UserId = Guid.NewGuid()};

            ATestUser.AddsTransaction(dto1);
            ATestUser.AddsTransaction(dto2);

            var transactions = ATestUser.GetsAllTransactions().ToList();

            Assert.AreEqual(2, transactions.Count());
            Assert.AreEqual(dto1.Transaction + dto2.Transaction, transactions.Sum(tx => tx.Transaction));
        }
    }
}
