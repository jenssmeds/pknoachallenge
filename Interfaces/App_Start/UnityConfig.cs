using System;
using System.Web.Http;
using System.Web.Mvc;
using Commons.Bus;
using Commons.DependencyInjection;
using Interfaces.EventBusListener;
using Interfaces.Hubs;
using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;
using Microsoft.Practices.Unity;
using Unity.Mvc5;

namespace Interfaces
{
    public static class UnityConfig
    {
        private static Lazy<IUnityContainer> container = new Lazy<IUnityContainer>(() =>
        {
            var container = new UnityContainer();
            return container;
        });

        public static IUnityContainer Container { get { return container.Value; } }

        public static void RegisterComponents()
        {
            Container.RegisterType<IEventListener, TransactionSummaryEventListener>(new ContainerControlledLifetimeManager());
            new CompositionRoot().ConfigureServices(Container);
            DependencyResolver.SetResolver(new UnityDependencyResolver(Container));
            GlobalConfiguration.Configuration.DependencyResolver = new Unity.WebApi.UnityDependencyResolver(Container);
            GlobalHost.DependencyResolver.Register(typeof(IHubActivator), () => new UnityHubActivator());

            Container.RegisterInstance(new TransactionsHubContext(GlobalHost.ConnectionManager.GetHubContext<TransactionsHub>().Clients));
        }

        public class UnityHubActivator : IHubActivator
        {
            public IHub Create(HubDescriptor descriptor)
            {
                return (IHub)DependencyResolver.Current
                    .GetService(descriptor.HubType);
            }
        }
    }
}