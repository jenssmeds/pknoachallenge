﻿using System.Web.Mvc;

namespace Interfaces.Controllers
{
    [RoutePrefix("Home")]
    public class HomeController : Controller
    {
        [Route("Display")]
        public ActionResult Display()
        {
            return View();
        }
    }
}
