﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Application.Commands.AddTransaction;
using Application.Queries.Transaction;
using Interfaces.Dtos;

namespace Interfaces.Controllers
{
    [RoutePrefix("api/transactions/")]
    public class TransactionsController : ApiController
    {
        readonly AddTransactionCommand addTransaction;
        readonly TransactionQuery transactionQuery;

        public TransactionsController(AddTransactionCommand addTransaction, TransactionQuery transactionQuery)
        {
            this.addTransaction = addTransaction;
            this.transactionQuery = transactionQuery;
        }

        [Route("")]
        public IEnumerable<TransactionDto> Get()
        {
            var transactions = transactionQuery.GetAll();
            return transactions.Select(tx => new TransactionDto() { Transaction = tx.Value, UserId = tx.UserId });
        }

        [Route("")]
        [HttpPost]
        public HttpResponseMessage Write([FromBody]TransactionDto transaction)
        {
            var request = new AddTransactionRequest(transaction.UserId, transaction.Transaction);

            addTransaction.Execute(request);

            return new HttpResponseMessage(HttpStatusCode.Created);
        }
    }
}