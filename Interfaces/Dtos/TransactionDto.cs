﻿using System;

namespace Interfaces.Dtos
{
    public class TransactionDto
    {
        public Guid UserId { get; set; }
        public double Transaction { get; set; }
    }
}