﻿namespace Interfaces.Dtos
{
    public class TransactionSummaryDto
    {
        public int Count { get; set; }
        public double Sum { get; set; }
    }
}