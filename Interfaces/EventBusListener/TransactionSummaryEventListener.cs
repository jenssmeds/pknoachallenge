﻿using System.Linq;
using Application;
using Application.Commands.AddTransaction;
using Application.Queries.Transaction;
using Commons.Bus;
using Interfaces.Dtos;
using Interfaces.Hubs;

namespace Interfaces.EventBusListener
{
    public class TransactionSummaryEventListener : IEventListener
    {
        readonly IEventBus eventBus;
        readonly TransactionQuery transactionQuery;
        readonly TransactionsHubContext transactionsHubContext;

        public TransactionSummaryEventListener(IEventBus eventBus, TransactionQuery transactionQuery, TransactionsHubContext transactionsHubContext)
        {
            this.eventBus = eventBus;
            this.transactionQuery = transactionQuery;
            this.transactionsHubContext = transactionsHubContext;
        }

        public void Subscribe()
        {
            eventBus.Subscribe<AddTransactionRequest>(BroadcastTransactionSummary);
        }

        private void BroadcastTransactionSummary(IEvent request)
        {
            var allTransactions = transactionQuery.GetAll();
            var dto = new TransactionSummaryDto() { Sum = allTransactions.Sum(tx => tx.Value), Count = allTransactions.Count() };
            transactionsHubContext.Clients.All.transactionSumBroadcast(dto);
        }
    }
}