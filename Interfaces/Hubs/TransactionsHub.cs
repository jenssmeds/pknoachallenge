﻿using System.Linq;
using Application.Queries.Transaction;
using Interfaces.Dtos;
using Microsoft.AspNet.SignalR;

namespace Interfaces.Hubs
{
    public class TransactionsHub : Hub
    {
        readonly TransactionQuery transactionQuery;

        public TransactionsHub(TransactionQuery transactionQuery)
        {
            this.transactionQuery = transactionQuery;
        }

        public TransactionSummaryDto GetTransactionSummary()
        {
            var allTransactions = transactionQuery.GetAll();
            return new TransactionSummaryDto() { Sum = allTransactions.Sum(tx => tx.Value), Count = allTransactions.Count() };
        }
    }
}