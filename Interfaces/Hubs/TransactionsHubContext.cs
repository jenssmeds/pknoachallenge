﻿using Microsoft.AspNet.SignalR.Hubs;

namespace Interfaces.Hubs
{
    public class TransactionsHubContext
    {
        public IHubConnectionContext<dynamic> Clients { get; set; }

        public TransactionsHubContext(IHubConnectionContext<dynamic> clients)
        {
            Clients = clients;
        }
    }
}