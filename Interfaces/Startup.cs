﻿using System.Web.Http;
using Commons.Bus;
using Microsoft.Owin;
using Microsoft.Practices.Unity;
using Owin;

[assembly: OwinStartup(typeof(Interfaces.Startup))]
namespace Interfaces
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            SetUpDependencyInjection();
            var config = new HttpConfiguration();
            config.MapHttpAttributeRoutes();
            app.MapSignalR();
            UnityConfig.Container.Resolve<IEventListener>().Subscribe();
        }

        protected virtual void SetUpDependencyInjection()
        {
            UnityConfig.RegisterComponents();
        }
    }
}