﻿using System.Collections.Generic;
using Domain.Transaction;
using ServiceStack.Redis;
using ServiceStack.Redis.Generic;

namespace Persistence
{
    public class TransactionRepository : ITransactionRepository
    {
        readonly IRedisTypedClient<ITransaction> transactionClient;

        public TransactionRepository(IRedisClient redisClient)
        {
            transactionClient = redisClient.As<ITransaction>();
        }

        public virtual IEnumerable<ITransaction> GetAll()
        {
            return transactionClient.GetAll();
        }

        public virtual void Save(ITransaction transaction)
        {
            transactionClient.Store(transaction);
        }
    }
}
